// JavaScript Document

function show_color() {

  let color = document.getElementById('color').value;

  if (color == "blue") {
    document.getElementById("color_box").style.backgroundColor = "blue";
    document.getElementById('text').style.color = "blue";
  } else if (color == "red") {
    document.getElementById("color_box").style.backgroundColor = "red";
    document.getElementById('text').style.color = "red";
  } else if (color == "green") {
    document.getElementById("color_box").style.backgroundColor = "green";
    document.getElementById('text').style.color = "green";
  } else if (color == "orange") {
    document.getElementById("color_box").style.backgroundColor = "orange";
    document.getElementById('text').style.color = "orange";
  } else if (color == "pink") {
    document.getElementById("color_box").style.backgroundColor = "pink";
    document.getElementById('text').style.color = "pink";
  } else if (color == "black") {
    document.getElementById("color_box").style.backgroundColor = "black";
    document.getElementById('text').style.color = "black";
  } else if (color == "magenta") {
    document.getElementById("color_box").style.backgroundColor = "magenta";
    document.getElementById('text').style.color = "magenta";
  } else if (color == "purple") {
    document.getElementById("color_box").style.backgroundColor = "purple";
    document.getElementById('text').style.color = "purple";
  } else if (color == "yellow") {
    document.getElementById("color_box").style.backgroundColor = "yellow";
    document.getElementById('text').style.color = "yellow";
  } else if (color == "white") {
    document.getElementById("color_box").style.backgroundColor = "white";
    document.getElementById('text').style.color = "white";
  } else if (color == "gray") {
    document.getElementById("color_box").style.backgroundColor = "gray";
    document.getElementById('text').style.color = "gray";
  } else if (color == "indigo") {
    document.getElementById("color_box").style.backgroundColor = "indigo";
    document.getElementById('text').style.color = "indigo";
  } else if (color == "gold") {
    document.getElementById("color_box").style.backgroundColor = "gold";
    document.getElementById('text').style.color = "gold";
  } else if (color == "silver") {
    document.getElementById("color_box").style.backgroundColor = "silver";
    document.getElementById('text').style.color = "silver";
  }


  document.getElementById('text').innerHTML = color;


}

function remove_color() {
  document.getElementById('color').value = '';
  document.getElementById('text').innerHTML = '';
  document.getElementById("color_box").style.backgroundColor = '';
}
