//JavaScript part..

// variable to store the list of guesses 
let guesses = [];
// variable for store the correct random number 
let correctNumber = getRandomNumber();

window.onload = function() {
    document.getElementById("number-submit").addEventListener("click", playGame);
    document.getElementById("restart-game").addEventListener("click", initGame);
}

/**
 * functions for playing the whole game
 */

function playGame(){
  let numberGuess = document.getElementById('number-guess').value;
  displayResult(numberGuess);
  saveGuessHistory(numberGuess);
  displayHistory();
}

// show the result for if the guess it too high, too low, or correct
function displayResult(numberGuess){
  if(numberGuess > correctNumber){
    showNumberAbove();
  }
  else if(numberGuess < correctNumber){
    showNumberBelow();
  }
  else if(numberGuess == correctNumber){
    showYouWon();
  } 
}


 // initialize a new game by resetting values and page
function initGame(){
  // reset: correctnumber, resultdisplay, guessesarray, historydisplay
  correctNumber = getRandomNumber();
  document.getElementById("result").innerHTML = "";
  guesses = [];
  displayHistory();

}

// reset the html content for guess history
function resetResultContent(){
  document.getElementById("result").innerHTML = "";
}

/**
 * return a random number 1-100
 * used Math.random 
 */
function getRandomNumber(){
  let randomNumber = Math.random();
  let wholeNumber = Math.floor(randomNumber * 100) + 1;
  return wholeNumber;
}

// save guess history. guesses variable
function saveGuessHistory(guess) {
  guesses.push(guess);
  console.log(guesses);
}

/**
* display guess history to user
* used html part, while loop and string concatentation to create a list
*/
function displayHistory() {
  let index = guesses.length - 1;
  let list = "<ul class='list-group'>";

  while(index >= 0){
    list += "<li class='list-group-item'>" + "You guessed " + 
    guesses[index] + "</li>";
    index -= 1;
  }
  list += '</ul>'
  document.getElementById("history").innerHTML = list;
}


// retrieve the dialog- if the guess is wrong or correct 
function getDialog(dialogType, text){
  let dialog;
  switch(dialogType){
    case "warning":
      dialog = "<div class='alert alert-warning' role='alert'>"
      break;
    case "won":
      dialog = "<div class='alert alert-success' role='alert'>"
      break;
  }
  dialog += text;
  dialog += "</div>"
  return dialog;
}

function showYouWon(){
  const text = "Awesome, You got your lucky number!"
  /**
   * retrieve the dialog using the getDialog() function
   * and save it to variable called dialog
   */
  let dialog = getDialog('won', text)
  document.getElementById("result").innerHTML = dialog;
}

function showNumberAbove(){
  const text = "too high!"
  /**
   * retrieve the dialog using the getDialog() function
   * and save it to variable called dialog
   */
  let dialog = getDialog('warning', text);
  document.getElementById("result").innerHTML = dialog;
}

function showNumberBelow(){
  const text = "too low!"
  /**
   * retrieve the dialog using the getDialog() function
   * and save it to variable called dialog
   */
  let dialog = getDialog('warning', text);
  document.getElementById("result").innerHTML = dialog;
}

